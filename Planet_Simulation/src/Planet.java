/**
 * Created by EunjinCho on 2016. 3. 8..
 */
public class Planet {
    public double xxPos;
    public double yyPos;
    public double xxVel;
    public double yyVel;
    public double mass;
    public String imgFileName;
    public double G = 6.67 * Math.pow(10, -11);

    public Planet(double xP, double yP, double xV,
                  double yV, double m, String img) {
        xxPos = xP;
        yyPos = yP;
        xxVel = xV;
        yyVel = yV;
        mass = m;
        imgFileName = img;
    }

    public Planet(Planet p) {
        xxPos = p.xxPos;
        yyPos = p.yyPos;
        xxVel = p.xxVel;
        yyVel = p.yyVel;
        mass = p.mass;
        imgFileName = p.imgFileName;
    }

    public double calcDistance(Planet anotherP) {
        double thisX = this.xxPos;
        double thisY = this.yyPos;
        double anotherX = anotherP.xxPos;
        double anotherY = anotherP.yyPos;

        double deltaX = Math.abs(thisX - anotherX);
        double deltaY = Math.abs(thisY - anotherY);
        double dist = (deltaX*deltaX) + (deltaY*deltaY);
        return Math.sqrt(dist);
    }

    // Calculate the force exerted on this planet by the given plane
    public double calcForceExertedBy(Planet givenP) {
        double dist = this.calcDistance(givenP);
        double thisM = this.mass;
        double givenM = givenP.mass;
        return G*thisM*givenM/(dist*dist);
    }

    // Calculate the force exerted in the X directions
    public double calcForceExertedByX(Planet givenP) {
        double totalF = calcForceExertedBy(givenP);
        double deltaX = Math.abs(this.xxPos - givenP.xxPos);
        double dist = calcDistance(givenP);
        return totalF*deltaX/dist;

    }

    // Calculate the force exerted in the Y directions
    public double calcForceExertedByY(Planet givenP) {
        double totalF = calcForceExertedBy(givenP);
        double deltaY = Math.abs(this.yyPos - givenP.yyPos);
        double dist = calcDistance(givenP);
        return totalF*deltaY/dist;
    }

    // take in an array of Planets
    // calculate the net X force exerted by all planets in that array
    // upon the current Planet.
    public double calcNetForceExertedByX(Planet[] arrP) {
        double total = 0.0;
        for (Planet planet : arrP) {
            if (this.equals(planet)) {
                continue;
            } else {
                double forceX = this.calcForceExertedByX(planet);
                total += forceX;
            }
        }
        return total;
    }

    // take in an array of Planets
    // calculate the net Y force exerted by all planets in that array
    // upon the current Planet.
    public double calcNetForceExertedByY(Planet[] arrP) {
        double total = 0.0;
        for (Planet planet : arrP) {
            if (this.equals(planet)) {
                continue;
            } else {
                double forceY = this.calcForceExertedByY(planet);
                total += forceY;
            }
        }
        return total;
    }

    // update how much the forces exerted on the planet will cause that planet to accelerate
    // and the resulting change in the planet's velocity and position in a small period of time
    // update the planet's position and velocity instances
    public void update(double dt, double fX, double fY) {
        // net acceleration
        double anetX = fX/mass;
        double anetY = fY/mass;

        // new velocity
        this.xxVel += (dt * anetX);
        this.yyVel += (dt * anetY);

        // new position
        this.xxPos += (dt * this.xxVel);
        this.yyPos += (dt * this.yyVel);

    }

    public void draw() {
        StdDraw.picture(xxPos, yyPos, imgFileName);
        //StdDraw.show();
    }
}
