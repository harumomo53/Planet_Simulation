Project0 - NBodySimulation
===

### Understanding the Physics
Our ```Planet``` object will obey the laws of Newtonian physics. They will be subject to:

* **Pairwise Force**: *Netwon's law of universal graviation* asserts that the strength of the gravitational force between 2 particles is given by the **product of their masses divided by the square of the distance between them**, scaled by the gravitational constant G. The gravitational force exerted on a particle is along the straight line between them.
    * Since we are using Cartesian coordinates to represent the position of a particle, it is convenient to break up the force into its x- and y-components (Fx, Fy). 

Note that force is a vector(has direction). dx and dy are signed(positive or negative)
* **Net Force**: The *principle of superposition* says that the net force acting on a particle in the x- or y- direction is the **sum of the pairwise forces acting on the particle in that direction**

* **Acceleration**: Newton's *second law of motion* says that the accelerations in the x- and y-directions are given by: 
    * ax = Fx/m
    * ay = Fy/m

#### Necessary computations
* Compute F1 (force that Saturn exerts on the Sun): we need to calculate r
* |F|=|F1|=|F2| 
* Compute F_(1,x) and F_(1,y) - the sign of dx and dy is positive
* Compute F2 (force that the Sun exerts on Saturn) - the values of dx and dy are negated
* Compute F_(2,x) and F_(2,y) - the sign of dx and dy is negative
* Compute x-component of the net force on Saturn by summing the x-components of all pairwise forces 
    * F_{net, x} = F_{1,x} + F_{2,x}
    * F_{net, y} = F_{1,y} + F_{2,y}

#### Writing methods
* the ```calcDistance(Planet p)``` method : calculates the distance between two Planets and returns a double

* the ```calcForceExertedBy(Planet p)``` method : returns a double describing the force exerted on this planet given by given planet

* the ```calcForceExertedByX(Planet p)```, ```calcForceExertedByY(Planet p)``` methods : Unlike the ```calcForceExertedBy``` method, which **returns the total force**, these 2 methods describe the **force exerted in the X and Y directions, respectively**

* the ```calcNetForceExertedByX(Planet[] arrP)```, ```calcNetForceExertedByY(Planet[] arrP)``` methods : take in an array of Planets and calculate the net X and net Y force exerted by all planetes in that array upon the current Planet
    * Planets cannot exert gravitational forces on themselves

#### update
We'll add a method that determines **how much the forces exerted on the planet will cause that planet to accelerate** and the resulting change in the planet's velcoity and position in a small period of time dt.

For example, ```samh.update(0.005, 10, 3)``` would adjust the velocity and position if an x-force of 10 Newtons and a y-force of 3 Newtons were applied for 0.005 seconds.

To compute the movement of the Planet: 

1. Calculate the acceleration using the provided x and y forces
2. Calculate the **new velcoity** by using the acceleration and current velocity. Recall that **accleration** = dv/dt; (**the change in velocity per unit time**). so the new velocity is (vx + dt * ax, vy + dt * ay).
3. Calculate the **new position** by using the velocity computed in step 2 and the current position. The new position is (px + dt * vx, py + dt * vy).

As a recap, it's important to know the following information.

1. The location -> the distance
2. The mass
3. (1,2) can give you Force, ForceExertedByX,Y, NetForceX,Y
4. NetForceX,Y and mass give you acceleration
5. Acceleration can give you new velocity
6. New velocity can give you new position

### Simulator
```NBody``` is a class that will actually run my simulation. This class doesn't have a constructor. The input format is a text file that contains the information for a particualr universe. 

1. An integer ```N``` which represents the number of planets
2. A real number ```R``` which represents the radius of the universe, used to determine the scaling of the drawing window
3. ```N``` rows and each row contains 6 values
    1. x- and y-coordinates of the initial position
    2. x- and y-components of the initial velocity
    3. mass
    4. String that is the name of the image file 

### Drawing the initial Universe state
We'll build the **functionality to draw the universe in its starting position**

#### Collecting All input
Created a ```main``` method in the ```NBody``` class. 
* Store the 0th and 1st command line arguments as doubles name ```T``` and ```dt```
* Store the 2nd command line argument as a String nmae ```filename```
* Read in the planets and the universe radius from the file described by ```filename``` using my methods written earlier

#### Drawing the Background
1. Set the scale so that it matches the radius of the universe
2. Draw the image ```starField.jpg``` as the background -> use the ```StdDraw``` library

* It was important to keep in mind that the background is of (-radius, radius)

#### Drawing one planet
We'll want a planet to be able to draw itself at its appropriate position. Add one method ```draw```, that uses the StdDraw API to draw the Planet's ```img``` at the Planet's position. 

### Creating an animation
At every discrete interval, we will be doing our calculation and once we have done our calculation for that time step, we will then update the values of our Planets and then redraw the universe.

1. Create a time variable and set it to 0. Loop until this time variable is T.
2. For each time through the loop:
    * Create an ```xForces``` array and ```yForces``` array
    * Calculate the net x and y forces for each planet, storing these in the xForces and yForces array respectively
    * Call update on each of the planets. This will update each planet's position, velocity, and acceleration.
    * Draw the background image.
    * Draw all of the planets.
    * Pause the animation for 10 milliseconds (see the show method of StdDraw). You may need to tweak this on your computer.
    * Increase your time variable by dt.














